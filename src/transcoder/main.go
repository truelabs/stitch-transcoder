package main

import (
	"encoding/json"
	"flag"
	"github.com/drichardson/appengine/pullqueue"
	"golang.org/x/net/context"
	"log"
	"time"
)

var project = flag.String("project", "stitch-1026", "Google Project ID")
var queueName = flag.String("queue", "transcode", "Taskqueue to consume.")
var leaseDuration = flag.Duration("lease-duration", 5*time.Minute, "Task lease duration.")
var dontLogTime = flag.Bool("dont-log-time", false, "If true, disable logging the time. Useful when run as systemd service.")

func main() {
	flag.Parse()
	logFlags := log.Lshortfile
	if !*dontLogTime {
		logFlags |= log.Ldate | log.Ltime
	}
	log.SetFlags(logFlags)

	c := context.Background()
	q := pullqueue.Queue{Project: *project, Name: *queueName}
	opts := pullqueue.Options{LeaseDuration: *leaseDuration, NoItemsLoopDelay: 2 * time.Second}

	q.Run(c, &opts, func(c context.Context, payload []byte) error {
		request := new(TranscodeOperationsRequest)
		if err := json.Unmarshal(payload, request); err != nil {
			log.Printf("Error unmarshalling json from payload. Error: %v", err)
			return err
		}

		if err := request.Process(c); err != nil {
			log.Printf("Error processing movie. Error: %v", err)
			return err
		}

		return nil
	})

	log.Fatal("Unexpected: end of main, should never get here.")
}
