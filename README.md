# Stitch Transcoder
Perform video transcodes, intended to support reduction in
video bitrate and also video thumbnail extraction.

# Building for Compute Engine

    cd build
    mkdir out
    ./build-transcoder out

This will produce a tarball in the `out` directory. You should upload
this to gs://stitch-opt/transcoder/ and then reference it in the
startup-script.

There is a script to build ffmpeg as well. Follow the same instructions as above
but using the `build-ffmpeg` script.

# Running
systemd runs the service. A service file `/opt/etc/systemd/system/transcoder.service`
defines how the service is run. The startup-script creates a symlink to this
file in `/etc/systemd/system`.

To start/stop the service use `systemctl`.

To view logs, use `sudo journalctl -f`

# Development
To run the service on a non-compute instance machine, you need to create
a JWT credential in Google Cloud Storage and then add the e-mail
address associated with this new credential to the stitch application's
thumbnail task queue as a reader.

Then, following the instructions on [Google Application Default Credentials](https://developers.google.com/identity/protocols/application-default-credentials)
to set the `GOOGLE_APPLICATION_CREDENTIALS` environment variable to point at the JWT file.
