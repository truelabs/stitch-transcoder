#!/bin/bash

#
# Command Line Arguments
#
INPUT=$1
shift
VIDEO_BITRATE=$1
shift
AUDIO_BITRATE=$1
shift
MAXWIDTH=$1
shift
MAXHEIGHT=$1
shift
OUTPUT=$1
shift

usage() {
    echo "Usage: transcode <input> <video_bitrate> <audio_bitrate> <max_width> <max_height> <output>"
    echo "Applies rotations specified by input metadata, aspect scales and"
    echo "encodes video and audio to provide decent quality while maximizing the"
    echo "number of platforms (android, ios) the video/audio can be played back on."
}

if [ -z "$INPUT" ]; then
    echo "Missing input"
    usage
    exit 1
fi

if [ -z "$VIDEO_BITRATE" ]; then
    echo "Missing video_bitrate"
    usage
    exit 1
fi

if [ -z "$AUDIO_BITRATE" ]; then
    echo "Missing audio_bitrate"
    usage
    exit 1
fi

if [ -z "$MAXWIDTH" ]; then
    echo "Missing max_width"
    usage
    exit 1
fi

if [ -z "$MAXHEIGHT" ]; then
    echo "Missing max_height"
    usage
    exit 1
fi

if [ -z "$OUTPUT" ]; then
    echo "Missing output"
    usage
    exit 1
fi

#
# Transcode video.
#

ffmpeg -y -nostdin \
    -i "$INPUT" \
    -filter:v "scale=width=trunc(iw*min(1\,min($MAXWIDTH/iw\,$MAXHEIGHT/ih))/2)*2:height=trunc(ih*min(1\,min($MAXWIDTH/iw\,$MAXHEIGHT/ih))/2)*2" \
    -c:v libx264 -profile:v baseline -preset veryfast -b:v "$VIDEO_BITRATE" \
    -c:a libfdk_aac -profile:a aac_he -b:a "$AUDIO_BITRATE"  \
    -r 30 \
    -pix_fmt +yuv420p \
    -f mp4 "$OUTPUT"

